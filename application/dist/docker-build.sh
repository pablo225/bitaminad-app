#!/bin/bash
cd "$(dirname "$0")"

registry=uhn-va-eng-002d.uhn.ca:5000
image=ems/uhn-smart-on-fhir-authz-server
jar=../target/uhn-smart-on-fhir-authz-server.war

DOCKER_TAG="${DOCKER_TAG:-latest}"

if [ ! -f $jar ]; then
	echo "${jar} not found. Did you forget to build it?"
	exit 1;
fi

docker build -t ${registry}/${image}:${DOCKER_TAG} -f .//Dockerfile ../
if [ "$DOCKER_TAG" != "latest" ]; then
	docker tag ${registry}/${image}:${DOCKER_TAG} ${registry}/${image}:latest
fi