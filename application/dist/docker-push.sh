#!/bin/bash
cd "$(dirname "$0")"

registry=uhn-va-eng-002d.uhn.ca:5000
image=ems/uhn-smart-on-fhir-authz-server

if [ ! -z "$DOCKER_USER" ] && [ ! -z "$DOCKER_PASS" ] ; then
    docker login -u $DOCKER_USER -p $DOCKER_PASS $registry
else
    echo "Skipping docker login because DOCKER_USER and DOCKER_PASS were not both set."
fi

docker push ${registry}/${image}
